import React from "react"
import { Box } from "grommet"
import { withRouter } from "react-router-dom"
import MenuItem from "./MenuItem"

const items = [
  {
    pathname: "/",
    name: "Accueil"
  },
  {
    pathname: "/beers",
    name: "Bières"
  },
  {
    pathname: "/users",
    name: "Utilisateurs"
  },
  {
    pathname: "/likes",
    name: "Ils aiment"
  }
]

const Nav = ({ location: { pathname }, history: { push } }) => {
  return (
    <Box pad="small">
      {items.map(i => (
        <MenuItem
          active={pathname === i.pathname}
          key={i.pathname}
          text={i.name}
          onClick={() => push(i.pathname)}
        />
      ))}
    </Box>
  )
}

export default withRouter(Nav)
