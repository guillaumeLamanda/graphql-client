import React, { Component } from "react"
import { InfiniteScroll, Box, Text } from "grommet"

class List extends Component {
  componentDidMount = () => {
    this.props.subscribeToNewLike()
  }

  render() {
    const { likes } = this.props
    return (
      <Box fill overflow="auto">
        <InfiniteScroll items={likes}>
          {like => (
            <Box pad="medium" key={`user-${like.name}-${like.beer.name}`}>
              <Text>
                {like.name} aime {like.beer.name}
              </Text>
            </Box>
          )}
        </InfiniteScroll>
      </Box>
    )
  }
}

export default List
