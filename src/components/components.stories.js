import React from "react"
import { storiesOf } from "@storybook/react"
import Layout from "./Layout"
import Header from "./Header"
import Footer from "./Footer"

storiesOf("Layout", module)
  .add("Combined components", () => <Layout />)
  .add("Header", () => <Header />)
  .add("Footer", () => <Footer />)
