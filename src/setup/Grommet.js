import React from "react"
import { Grommet } from "grommet"

import brestJSTheme from "./theme.json"

const GrommetSetup = ({ children }) => {
  return (
    <Grommet theme={brestJSTheme} full>
      {children}
    </Grommet>
  )
}

export default GrommetSetup
