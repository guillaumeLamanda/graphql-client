import React, { Component } from "react"
import "./App.css"
import { GrommetSetup, ApolloSetup } from "./setup"
import { Layout } from "./components"

class App extends Component {
  render() {
    return (
      <ApolloSetup>
        <GrommetSetup>
          <Layout />
        </GrommetSetup>
      </ApolloSetup>
    )
  }
}

export default App
