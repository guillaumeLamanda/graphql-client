import React from "react"
import { Query } from "react-apollo"
import gql from "graphql-tag"
import List from "./List"

/**
 * Doc des souscriptions : https://www.apollographql.com/docs/react/advanced/subscriptions.html#overview
 */

const BEER_LIKE_SUBSCRIPTION = gql`
  subscription userBeerLiked {
    userBeerLiked {
      name
      beer {
        id
        name
      }
    }
  }
`

const BEER_LIKE_QUERY = gql`
  {
    usersBeersLikes {
      name
      beer {
        id
        name
      }
    }
  }
`

const Container = () => {
  return (
    <Query query={BEER_LIKE_QUERY}>
      {({ loading, data: { usersBeersLikes }, subscribeToMore }) => {
        return (
          <List
            likes={loading ? [] : usersBeersLikes}
            subscribeToNewLike={() =>
              subscribeToMore({
                document: BEER_LIKE_SUBSCRIPTION,
                updateQuery: (prev, { subscriptionData }) => {
                  if (
                    !subscriptionData.data ||
                    (subscriptionData.data &&
                      !subscriptionData.data.userBeerLiked)
                  )
                    return prev
                  const newLike = subscriptionData.data.userBeerLiked

                  return {
                    usersBeersLikes: [newLike, ...prev.usersBeersLikes]
                  }
                },
                onError: e => console.log(e)
              })
            }
          />
        )
      }}
    </Query>
  )
}

export default Container
