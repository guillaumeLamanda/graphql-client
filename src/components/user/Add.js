import React from "react"
import PropTypes from "prop-types"
import { Form, TextInput, Button, Box } from "grommet"

class Add extends React.Component {
  static propTypes = {
    addUser: PropTypes.func.isRequired
  }

  static defaultProps = {
    addUser: () => {
      console.log(this.inputRef.current.value)
    }
  }

  inputRef = React.createRef()

  render() {
    const { addUser } = this.props
    return (
      <Box pad="medium" direction="row" gap="medium" justify="center">
        <Form>
          <TextInput ref={this.inputRef} />
        </Form>
        <Button
          onClick={() => {
            addUser(this.inputRef.current.value)
            this.inputRef.current.value = ""
          }}
        >
          Ajouter
        </Button>
      </Box>
    )
  }
}

export default Add
