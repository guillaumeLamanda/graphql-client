import React from "react"
import { configure, addDecorator } from "@storybook/react"
import { GrommetSetup } from "../src/setup"

/** Add grommet setup */
addDecorator(story => <GrommetSetup>{story()}</GrommetSetup>)

/** Require all stories */
const req = require.context("../src/components", true, /\.stories\.js$/)

function loadStories() {
  req.keys().forEach(filename => req(filename))
}

configure(loadStories, module)
