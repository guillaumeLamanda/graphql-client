# Atelier BrestJS - GraphQL/Apollo

Bonjour à tous et bienvenu à l'atelier BrestJS, dans lequel nous allons voir la mise en place d'un client Apollo.

## L'environnement

Personnellement, je travail sous vscode, et j'ai plusieurs extensions qui apportent une fluidité dans le développement :

- [Snippets](https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets),
- [GraphQL for vscode](https://marketplace.visualstudio.com/items?itemName=kumar-harsh.graphql-for-vscode)
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode), pour formatter notre code,
- [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

## Structure

Le projet est divisé en plusieurs étapes sous forme de branche. Chaque branche contient son README avec ses instructions pour réaliser l'étape.

Si jamais vous êtes bloqué à une étape, et vous voulez à la prochaine, procédez ainsi :

- Sauvegardez vos modifications, vous pourrez ainsi y revenir plus tard :
  - `git add .`
  - `git commit -m "mon message"`
- Et passez sur la branche suivante :
  - `git checkout stepx-nomDeLaBrancheSuivante`

Vous êtes prêt à y aller, allez voir le README de la step1 🚀
