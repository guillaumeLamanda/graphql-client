import React from "react"
import { Box, Heading } from "grommet"

const Header = () => {
  return (
    <Box
      align="center"
      background="brand"
      direction="row"
      fill
      justify="between"
      pad={{ left: "small", right: "small" }}
    >
      <Heading margin="xxsmall">BrestJS</Heading>
      <Heading level="5" margin="xxsmall">
        Atelier graphQL
      </Heading>
    </Box>
  )
}

export default Header
