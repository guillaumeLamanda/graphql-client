import React from "react"
import PropTypes from "prop-types"
import { Box, InfiniteScroll } from "grommet"
import ListItem from "./ListItem"

class List extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      beers: props.beers
    }
  }

  /**
   * Fonction appelé quand on reçoit des bières supplémentaires
   * On met à jour notre tableau de bière dans l'état du composant
   * en fusionnant les bières de l'état avec les nouvelles bières
   * reçu en props.
   * On filtre ensuite les bières en doublon dans ce tableau.
   */
  static getDerivedStateFromProps(props, state) {
    const { beers } = state
    return {
      beers: [...props.beers, ...beers]
        .reduce(
          (accu, beer) =>
            accu.some(b => b.id === beer.id) ? [...accu] : [...accu, beer],
          []
        )
        .sort((a, b) => a.id - b.id)
    }
  }

  render() {
    const { onMore, onLikeClick } = this.props
    const { beers } = this.state

    return (
      <Box fill overflow="auto">
        <InfiniteScroll items={beers} onMore={onMore} step={10}>
          {beer => (
            <ListItem
              key={`beer-${beer.name}`}
              description={`${beer.degree}°`}
              name={beer.name}
              liked={beer.liked}
              onLikeClick={() => onLikeClick(beer.id)}
            />
          )}
        </InfiniteScroll>
      </Box>
    )
  }
}

List.propTypes = {
  beers: PropTypes.array.isRequired
}

List.defaultProps = {
  beers: []
}

export default List
