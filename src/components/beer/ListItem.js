import React from "react"
import PropTypes from "prop-types"
import { Box, Text, Button } from "grommet"
import { Favorite } from "grommet-icons"

const ListItem = ({ name, description, liked, onLikeClick }) => {
  return (
    <Box direction="row" justify="between" margin="medium" pad="medium">
      <Box justify="center" fill="vertical">
        <Text weight="bold">{name}</Text>
        <Text size="small">{description}</Text>
      </Box>
      <Button
        alignSelf="center"
        onClick={onLikeClick}
        icon={<Favorite color={liked ? "brand" : "dark-4"} />}
      />
    </Box>
  )
}

ListItem.propTypes = {
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  onLikeClick: PropTypes.func.isRequired,
  liked: PropTypes.bool.isRequired
}

ListItem.defaultProps = {
  onLikeClick: b => console.log("beer liked", b),
  liked: false
}

export default ListItem
