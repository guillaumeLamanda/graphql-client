import React from "react"
import { Query, Mutation } from "react-apollo"
import gql from "graphql-tag"
import List from "./List"
import Add from "./Add"

const GET_USERS = gql`
  {
    users {
      id
      name
    }
  }
`

const ADD_USER = gql`
  mutation AddUser($name: String!) {
    addUser(name: $name) {
      id
      name
      token
    }
  }
`

const Container = props => {
  return (
    <>
      <Mutation mutation={ADD_USER} refetchQueries={[{ query: GET_USERS }]}>
        {addUser => (
          <Add
            addUser={async name => {
              const {
                data: {
                  addUser: { token }
                }
              } = await addUser({
                variables: { name }
              })
              localStorage.setItem("token", token)
            }}
          />
        )}
      </Mutation>
      <Query query={GET_USERS}>
        {({ loading, data: { users } }) => !loading && <List users={users} />}
      </Query>
    </>
  )
}

Container.propTypes = {}

export default Container
