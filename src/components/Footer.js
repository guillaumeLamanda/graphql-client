import React from "react"
import { Box, Text, Anchor } from "grommet"

const Footer = () => {
  return (
    <Box background="dark-1" fill justify="center" align="center">
      <Text>
        Avec ❤ par <Anchor primary label={"Guillaume Lamanda"} />, & toi.
      </Text>
    </Box>
  )
}

export default Footer
