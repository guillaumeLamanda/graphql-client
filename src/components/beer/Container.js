import React from "react"
import gql from "graphql-tag"
import { compose, graphql } from "react-apollo"
import { Query } from "react-apollo"
import BeerList from "./List"

const GET_BEERS = gql`
  query Beers($pageSize: Int!, $page: Int!) {
    beers(pageSize: $pageSize, page: $page) {
      id
      name
      degree
    }
  }
`

const USER_QUERY = gql`
  query User {
    user {
      id
      beers {
        id
      }
    }
  }
`

const TOOGLE_LIKE = gql`
  mutation ToogleLike($beerId: Int!) {
    toogleUserBeer(beerId: $beerId) {
      id
      beers {
        id
      }
    }
  }
`

class Container extends React.PureComponent {
  state = {
    page: 1
  }

  /**
   * Implement user beer query
   */

  onMore = () => this.setState({ page: this.state.page + 1 })

  isBeerLiked = beerId => {
    const {
      userQuery: { user }
    } = this.props
    return user && user.beers.some(e => e.id === beerId)
  }

  formatBeers = beers =>
    beers &&
    beers.reduce(
      (accu, beer) => [
        ...accu,
        {
          ...beer,
          liked: this.isBeerLiked(beer.id)
        }
      ],
      []
    )

  render() {
    const { page } = this.state

    /** On récupère notre mutation dans les props */
    const { toogleBeerLike } = this.props

    return (
      <Query query={GET_BEERS} variables={{ pageSize: 50, page }}>
        {({ data: { beers } }) => (
          <BeerList
            beers={this.formatBeers(beers)}
            onMore={this.onMore}
            /**
             * @name onLikeClick
             * @param {Int} beerId
             * @return {Promise}
             */
            onLikeClick={beerId =>
              toogleBeerLike({
                variables: {
                  beerId
                }
              })
            }
          />
        )}
      </Query>
    )
  }
}

export default compose(
  graphql(TOOGLE_LIKE, { name: "toogleBeerLike" }),
  graphql(USER_QUERY, { name: "userQuery" })
)(Container)
