import React from "react"
import { Box, Markdown } from "grommet"
import content from "./content/step1.md"

const Home = () => {
  return (
    <Box fill>
      <Markdown>{content}</Markdown>
    </Box>
  )
}

export default Home
