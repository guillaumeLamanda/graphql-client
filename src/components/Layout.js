import React from "react"
import { Box, Grid } from "grommet"
import { BrowserRouter as Router, Route } from "react-router-dom"
import Header from "./Header"
import Footer from "./Footer"
import { Home } from "./home"
import { Nav } from "./nav"
import { BeerListConnected } from "./beer"
import { UserContentConnected } from "./user"
import { LikeConnected } from "./like"

const Layout = props => {
  return (
    <Router>
      <Grid
        fill
        gap="small"
        columns={["small", "flex"]}
        rows={["xsmall", "flex", "xsmall"]}
        areas={[
          { name: "header", start: [0, 0], end: [1, 0] },
          { name: "nav", start: [0, 1], end: [0, 1] },
          { name: "content", start: [1, 1], end: [1, 1] },
          { name: "footer", start: [0, 2], end: [1, 2] }
        ]}
      >
        <Box gridArea="header">
          <Header />
        </Box>
        <Box gridArea="nav" fill="vertical" background="light-5">
          <Nav />
        </Box>
        <Box gridArea="content" background="light-1">
          <Route path="/" exact component={Home} />
          <Route path="/beers" exact component={BeerListConnected} />
          <Route path="/users" extact component={UserContentConnected} />
          <Route path="/likes" extact component={LikeConnected} />
        </Box>
        <Box gridArea="footer">
          <Footer />
        </Box>
      </Grid>
    </Router>
  )
}

export default Layout
