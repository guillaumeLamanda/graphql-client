import React from "react"
import { storiesOf } from "@storybook/react"
import ListItem from "./ListItem"
import List from "./List"
import { beers } from "./data"

storiesOf("Beer", module)
  .add("ListItem", () => <ListItem name="XI.I" liked={true} />)
  .add("List", () => <List beers={beers} />)
