import React from "react"
import { ApolloProvider } from "react-apollo"
import {
  HttpLink,
  ApolloLink,
  ApolloClient,
  concat,
  InMemoryCache,
  split
} from "apollo-boost"
import { WebSocketLink } from "apollo-link-ws"
import { getMainDefinition } from "apollo-utilities"

/**
 * Setup Apollo client provider
 * More configuration here : https://www.apollographql.com/docs/react/basics/setup.html
 */
const httpLink = new HttpLink({ uri: "http://localhost:3000" })
const authMiddleware = new ApolloLink((operation, forward) => {
  const token = localStorage.getItem("token")

  token &&
    operation.setContext({
      headers: {
        authorization: token
      }
    })
  return forward(operation)
})

const wsLink = new WebSocketLink({
  uri: "ws://localhost:3000/graphql",
  options: {
    reconnect: true
  }
})

const link = split(
  // split based on operation type
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query)
    return kind === "OperationDefinition" && operation === "subscription"
  },
  wsLink,
  httpLink
)

const client = new ApolloClient({
  link: concat(authMiddleware, link),
  cache: new InMemoryCache()
})

const SetupApollo = ({ children }) => {
  return <ApolloProvider client={client}>{children}</ApolloProvider>
}

export default SetupApollo
