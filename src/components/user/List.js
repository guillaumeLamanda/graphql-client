import React from "react"
import { InfiniteScroll, Box, Text } from "grommet"

const List = ({ users }) => (
  <Box fill overflow="auto">
    <InfiniteScroll items={users} step={6}>
      {user => (
        <Box key={`user-${user.id}`} pad="small" margin="medium">
          <Text weight="bold">{user.name}</Text>
        </Box>
      )}
    </InfiniteScroll>
  </Box>
)
export default List
