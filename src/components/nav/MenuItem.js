import React from "react"
import PropTypes from "prop-types"
import { Box, Text, Button } from "grommet"

const MenuItem = ({ text, active, onClick }) => {
  return (
    <Button
      hoverIndicator
      active={active}
      margin="small"
      round="small"
      onClick={onClick}
      color="brand"
    >
      <Box pad="small">
        <Text color={active ? "brand" : null}>{text}</Text>
      </Box>
    </Button>
  )
}

MenuItem.propTypes = {
  text: PropTypes.string.isRequired,
  active: PropTypes.bool,
  onClick: PropTypes.func
}

export default MenuItem
